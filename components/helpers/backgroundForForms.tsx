import React, { FC, HTMLAttributes } from "react";
import Loader from "../helpers/loader";
import backgroundLoader from "./backgroundLoader.module.scss";

interface IProps extends HTMLAttributes<HTMLElement> {
  showLoader: boolean;
  message: string;
}

export const BackgroundLoader: FC<IProps> = ({ showLoader, message }) => (
  <div className={backgroundLoader.loaderContainer}>
    {showLoader && <Loader />}
    <h2>{message}</h2>
  </div>
);

export default BackgroundLoader;
