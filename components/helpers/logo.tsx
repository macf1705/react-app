import React, { FC } from "react";
import style from "./logo.module.scss";

const Logo: FC = () => {
  return <img src="/ORM-Logo.png" alt="ORM Logo" className={style.logo} />;
};

export default Logo;
