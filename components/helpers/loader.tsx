import React, { FC } from "react";
import loader from "./loader.module.scss";

const Loader: FC = () => {
  return (
    <div className={loader.spinner}>
      <div></div>
      <div></div>
      <div></div>
      <div></div>
      <div></div>
      <div></div>
      <div></div>
      <div></div>
      <div></div>
      <div></div>
      <div></div>
      <div></div>
    </div>
  );
};

export default Loader;
