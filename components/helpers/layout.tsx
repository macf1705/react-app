import Head from "next/head";
import Logo from "./logo";
import style from "./layout.module.scss";

export default function Layout({
  children,
  pageTitle = "This is the default title",
  headerTitle = "This is the default title",
}) {
  return (
    <div className={style.content}>
      <Head>
        <title>{pageTitle}</title>
        <meta charSet="utf-8" />
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
        <meta property="og:title" content={pageTitle} key="title" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <Logo />

      <main className={style.sectionForm}>
        <h1 className={style.titleHeader}>{headerTitle}</h1>

        {children}
      </main>
    </div>
  );
}
