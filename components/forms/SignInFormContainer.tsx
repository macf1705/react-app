import React, { Component, ChangeEvent } from "react";
import Input from "./forms-fields/input";
import Button from "./forms-fields/button";
import BackgroundLoader from "../helpers/backgroundForForms";
import form from "./SignInFormContainer.module.scss";

interface IMyComponentProps {
  emailDefaultValue: string;
  passwordDefaultValue: string;
}

interface IMyComponentState {
  email: string;
  password: string;
  errors: IMyComponentErrors;
  loader: IMyComponentLoader;
}

interface IMyComponentErrors {
  emailError: string;
  passwordError: string;
}

interface IMyComponentLoader {
  isSending: boolean;
  sendingMessage: string;
  showLoader: boolean;
}

const validEmailRegex = RegExp(
  /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i
);

const validateForm = (errors: IMyComponentErrors): boolean => {
  let valid = true;
  Object.values(errors).forEach((val) => val.length > 0 && (valid = false));
  return valid;
};

export default class SignInFormContainer extends Component<
  IMyComponentProps,
  IMyComponentState
> {
  constructor(props: IMyComponentProps) {
    super(props);
    this.state = {
      email: this.props.emailDefaultValue,
      password: this.props.passwordDefaultValue,
      errors: {
        emailError: "",
        passwordError: "",
      },
      loader: {
        isSending: false,
        sendingMessage: "Please wait...",
        showLoader: true,
      },
    };

    this.handleFormSubmit = this.handleFormSubmit.bind(this);
    this.handleClearForm = this.handleClearForm.bind(this);
    this.handleChangeEmail = this.handleChangeEmail.bind(this);
    this.handleChangePassword = this.handleChangePassword.bind(this);
  }

  handleChangeEmail(event: ChangeEvent<HTMLInputElement>): void {
    event.preventDefault();
    const { value } = event.target;
    let errors = this.state.errors;

    if (!validEmailRegex.test(value)) {
      errors.emailError = "Invalid e-mail address";
    } else if (!value) {
      errors.emailError = "Email is required";
    } else {
      errors.emailError = "";
    }

    this.setState({ errors, email: value });
  }

  handleChangePassword(event: ChangeEvent<HTMLInputElement>): void {
    event.preventDefault();
    const { value } = event.target;
    let errors = this.state.errors;

    if (!value) {
      errors.passwordError = "Password is required";
    } else {
      errors.passwordError = "";
    }

    this.setState({ errors, password: value });
  }

  handleClearForm() {
    this.setState({
      email: "",
      password: "",
    });
  }

  handleFormSubmit(event) {
    event.preventDefault();
    const { email, password } = this.state;
    let errors = this.state.errors;

    if (!email) {
      errors.emailError = "Email is required";
    }
    if (!password) {
      errors.passwordError = "Password is required";
    }

    this.setState({ errors });

    if (!validateForm(this.state.errors)) {
      return;
    } else {
      let loader = this.state.loader;
      loader.isSending = true;
      this.setState({ loader });

      let userData = {
        email,
        password,
      };
      fetch("http://example.com", {
        method: "POST",
        body: JSON.stringify(userData),
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
      })
        .then((response) => {
          this.handleClearForm();

          if (response.ok) {
            return response.json();
          } else {
            throw new Error("error message");
          }
        })
        .then((data) => {
          loader.sendingMessage = "Thank you for register new account.";
          loader.showLoader = false;
          this.setState({ loader });

          setTimeout(() => {
            loader.isSending = false;
            this.setState({ loader });
          }, 3000);
        })
        .catch((error) => {
          loader.sendingMessage = error;
          loader.showLoader = false;
          this.setState({ loader });

          setTimeout(() => {
            loader.isSending = false;
            this.setState({ loader });
          }, 3000);
        });
    }
  }

  render() {
    const { errors } = this.state;

    return (
      <form
        aria-label="Sign up form"
        autoComplete="off"
        className={form.formDefault}
        onSubmit={this.handleFormSubmit}
      >
        {this.state.loader.isSending && (
          <BackgroundLoader
            showLoader={this.state.loader.showLoader}
            message={this.state.loader.sendingMessage}
          />
        )}
        <Input
          type={"email"}
          title={"Email address"}
          name={"email"}
          placeholder={"Enter your email address"}
          value={this.state.email}
          error={errors.emailError.length > 0}
          errorMessage={errors.emailError}
          onChange={this.handleChangeEmail}
          required
        />
        <Input
          type={"password"}
          title={"Password"}
          name={"password"}
          placeholder={"Enter your password"}
          value={this.state.password}
          error={errors.passwordError.length > 0}
          errorMessage={errors.passwordError}
          onChange={this.handleChangePassword}
          required
        />
        <Button
          onClick={this.handleFormSubmit}
          title={"Continue Button"}
          children={"Continue"}
        />
      </form>
    );
  }
}
