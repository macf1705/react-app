import React, { ChangeEvent, InputHTMLAttributes, FC } from "react";
import inputStyles from "./input.module.scss";

interface IProps
  extends Omit<
    InputHTMLAttributes<HTMLInputElement>,
    "onChange" | "value" | "title" | "name"
  > {
  value: string;
  name: string;
  title: string;
  error?: boolean;
  errorMessage?: string;

  onChange(event: ChangeEvent<HTMLInputElement>): void;
}

const Input: FC<IProps> = ({
  children,
  value,
  title,
  name,
  error,
  errorMessage,
  onChange,
  ...shared
}) => {
  return (
    <fieldset className={inputStyles.fieldsetDefault}>
      <legend className={inputStyles.legendDefault}>{title}</legend>
      <label htmlFor={name}>{title}</label>
      <input
        onChange={onChange}
        id={name}
        className={`
          ${inputStyles.inputDefault} ${
          error
            ? inputStyles.requiredField
            : value
            ? inputStyles.correctFill
            : ""
        }`}
        {...shared}
      />
      {error && <div className={inputStyles.fieldAlert}>{errorMessage}</div>}
    </fieldset>
  );
};

Input.defaultProps = {
  type: "text",
};

export default Input;
