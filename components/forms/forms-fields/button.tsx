import React, { ButtonHTMLAttributes, FC } from "react";
import buttonStyles from "./button.module.scss";

interface IProps extends ButtonHTMLAttributes<HTMLButtonElement> {
  block?: true;
}

const Button: FC<IProps> = ({ block, children, ...shared }) => {
  return (
    <button {...shared} className={buttonStyles.submit}>
      {children}
    </button>
  );
};

Button.defaultProps = {
  type: "submit",
};

export default Button;
