import React, { Component, ChangeEvent } from "react";
import Input from "./forms-fields/input";
import Button from "./forms-fields/button";
import BackgroundLoader from "../helpers/backgroundForForms";
import form from "./SignUpFormContainer.module.scss";

interface IMyComponentProps {
  emailDefaultValue: string;
  fullNameDefaultValue: string;
  passwordDefaultValue: string;
  confirmPasswordDefaultValue: string;
}

interface IMyComponentState {
  email: string;
  fullName: string;
  password: string;
  confirmPassword: string;
  errors: IMyComponentErrors;
  loader: IMyComponentLoader;
}

interface IMyComponentErrors {
  emailError: string;
  fullNameError: string;
  passwordError: string;
  confirmPasswordError: string;
}

interface IMyComponentLoader {
  isSending: boolean;
  sendingMessage: string;
  showLoader: boolean;
}

const validEmailRegex = RegExp(
  /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i
);

const validFullNameRegex = RegExp(
  /^[a-z]([-']?[a-z]+)*( [a-z]([-']?[a-z]+)*)+$/i
);

const validateForm = (errors: IMyComponentErrors): boolean => {
  let valid = true;
  Object.values(errors).forEach((val) => val.length > 0 && (valid = false));
  return valid;
};

export default class SignUpFormContainer extends Component<
  IMyComponentProps,
  IMyComponentState
> {
  constructor(props: IMyComponentProps) {
    super(props);
    this.state = {
      email: this.props.emailDefaultValue,
      fullName: this.props.fullNameDefaultValue,
      password: this.props.passwordDefaultValue,
      confirmPassword: this.props.confirmPasswordDefaultValue,
      errors: {
        emailError: "",
        fullNameError: "",
        passwordError: "",
        confirmPasswordError: "",
      },
      loader: {
        isSending: false,
        sendingMessage: "Please wait...",
        showLoader: true,
      },
    };

    this.handleFormSubmit = this.handleFormSubmit.bind(this);
    this.handleClearForm = this.handleClearForm.bind(this);
    this.handleChangeEmail = this.handleChangeEmail.bind(this);
    this.handleChangeFullName = this.handleChangeFullName.bind(this);
    this.handleChangePassword = this.handleChangePassword.bind(this);
    this.handleChangeConfirmPassword = this.handleChangeConfirmPassword.bind(
      this
    );
  }

  handleChangeEmail(event: ChangeEvent<HTMLInputElement>): void {
    event.preventDefault();
    const { value } = event.target;
    let errors = this.state.errors;

    if (!validEmailRegex.test(value)) {
      errors.emailError = "Invalid e-mail address";
    } else if (!value) {
      errors.emailError = "Email is required";
    } else {
      errors.emailError = "";
    }

    this.setState({ errors, email: value });
  }

  handleChangeFullName(event: ChangeEvent<HTMLInputElement>): void {
    event.preventDefault();
    const { value } = event.target;
    let errors = this.state.errors;

    if (!validFullNameRegex.test(value)) {
      errors.fullNameError = "Missing last name";
    } else if (!value) {
      errors.fullNameError = "Full Name is required";
    } else {
      errors.fullNameError = "";
    }

    this.setState({ errors, fullName: value });
  }

  handleChangePassword(event: ChangeEvent<HTMLInputElement>): void {
    event.preventDefault();
    const { value } = event.target;
    let errors = this.state.errors;

    if (value.length < 8) {
      errors.passwordError = "Password is too short.";
    } else if (!value) {
      errors.passwordError = "Password is required";
    } else {
      errors.passwordError = "";
    }

    this.setState({ errors, password: value });
  }

  handleChangeConfirmPassword(event: ChangeEvent<HTMLInputElement>): void {
    event.preventDefault();
    const { value } = event.target;
    let errors = this.state.errors;

    if (value !== this.state.password) {
      errors.confirmPasswordError = "Passwords must match";
    } else if (!value) {
      errors.confirmPasswordError = "Confirm Password is required";
    } else {
      errors.confirmPasswordError = "";
    }

    this.setState({ errors, confirmPassword: value });
  }

  handleClearForm() {
    this.setState({
      email: "",
      fullName: "",
      password: "",
      confirmPassword: "",
    });
  }

  handleFormSubmit(event) {
    event.preventDefault();
    const { email, fullName, password, confirmPassword } = this.state;
    let errors = this.state.errors;

    if (!email) {
      errors.emailError = "Email is required";
    }
    if (!fullName) {
      errors.fullNameError = "Full Name is required";
    }
    if (!password) {
      errors.passwordError = "Password is required";
    }
    if (!confirmPassword) {
      errors.confirmPasswordError = "Confirm Password is required";
    }

    this.setState({ errors });

    if (!validateForm(this.state.errors)) {
      return;
    } else {
      let loader = this.state.loader;
      loader.isSending = true;
      this.setState({ loader });

      let userData = {
        email,
        fullName,
        password,
        confirmPassword,
      };

      fetch("http://example.com", {
        method: "POST",
        body: JSON.stringify(userData),
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
        },
      })
        .then((response) => {
          this.handleClearForm();

          if (response.ok) {
            return response.json();
          } else {
            throw new Error("error message");
          }
        })
        .then((data) => {
          loader.sendingMessage = "Thank you for register new account.";
          loader.showLoader = false;
          this.setState({ loader });

          setTimeout(() => {
            loader.isSending = false;
            this.setState({ loader });
          }, 3000);

          console.log(data);
        })
        .catch((error) => {
          loader.sendingMessage = error;
          loader.showLoader = false;
          this.setState({ loader });

          setTimeout(() => {
            loader.isSending = false;
            this.setState({ loader });
          }, 3000);

          console.log(error);
        });
    }
  }

  render() {
    const { errors } = this.state;

    return (
      <form
        aria-label="Sign up form"
        autoComplete="off"
        className={form.formDefault}
        onSubmit={this.handleFormSubmit}
      >
        {this.state.loader.isSending && (
          <BackgroundLoader
            showLoader={this.state.loader.showLoader}
            message={this.state.loader.sendingMessage}
          />
        )}

        <Input
          type={"email"}
          title={"Email address"}
          name={"email"}
          placeholder={"Enter your email address"}
          value={this.state.email}
          error={errors.emailError.length > 0}
          errorMessage={errors.emailError}
          onChange={this.handleChangeEmail}
          required
        />
        <Input
          type={"text"}
          title={"Full Name"}
          name={"fullName"}
          placeholder={"Enter your full name"}
          value={this.state.fullName}
          error={errors.fullNameError.length > 0}
          errorMessage={errors.fullNameError}
          onChange={this.handleChangeFullName}
          required
        />
        <Input
          type={"password"}
          title={"Password"}
          name={"password"}
          placeholder={"Enter your password"}
          value={this.state.password}
          error={errors.passwordError.length > 0}
          errorMessage={errors.passwordError}
          onChange={this.handleChangePassword}
          required
        />
        <Input
          type={"password"}
          title={"Confirm password"}
          name={"confirmPassword"}
          placeholder={"Confirm password"}
          value={this.state.confirmPassword}
          error={errors.confirmPasswordError.length > 0}
          errorMessage={errors.confirmPasswordError}
          onChange={this.handleChangeConfirmPassword}
          required
        />
        <Button
          onClick={this.handleFormSubmit}
          title={"Continue Button"}
          children={"Continue"}
        />
      </form>
    );
  }
}
