import Head from "next/head";
import Link from "next/link";
import SignInFormContainer from "../components/forms/SignInFormContainer";
import Layout from "./../components/helpers/layout";
import style from "../styles/default.module.scss";

export default function Home() {
  return (
    <Layout pageTitle="React App - Sign In" headerTitle="Sign in">
      <SignInFormContainer emailDefaultValue={""} passwordDefaultValue={""} />

      <p className={style.signUpLink}>
        Not our member yet?&nbsp;
        <Link href="/sign-up">
          <button aria-label="Create new account" className={style.btnLink}>
            Click here to create new account
          </button>
        </Link>
      </p>
    </Layout>
  );
}
