import Head from "next/head";
import style from "../styles/default.module.scss";

export default function Home() {
  return (
    <div className={style.content}>
      <Head>
        <title>React App</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
    </div>
  );
}
