import Link from "next/link";
import SignUpFormContainer from "./../components/forms/SignUpFormContainer";
import Layout from "./../components/helpers/layout";
import style from "../styles/default.module.scss";

export default function SignUp() {
  return (
    <Layout pageTitle="React App - Sign Up" headerTitle="Sign up">
      <SignUpFormContainer
        emailDefaultValue={""}
        fullNameDefaultValue={""}
        passwordDefaultValue={""}
        confirmPasswordDefaultValue={""}
      />

      <p className={style.signUpLink}>
        Already using our app?&nbsp;
        <Link href="/sign-up">
          <button aria-label="Create new account" className={style.btnLink}>
            Click here to sign in
          </button>
        </Link>
      </p>
    </Layout>
  );
}
